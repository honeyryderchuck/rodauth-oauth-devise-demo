class ApplicationController < ActionController::Base

  def authenticate_user!
    rodauth.session_value || super
  end

end
