class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :require_read_access, only: [:index, :show]
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  def index
    account = current_user || current_oauth_account
    @posts = account.posts.all
    respond_to do |format|
      format.html
      format.json { render json: @posts.to_json }
    end
  end

  # GET /posts/1
  def show
  end

  # GET /posts/new
  def new
    @post = current_user.posts.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  def create
    @post = current_user.posts.new(post_params)

    if @post.save
      redirect_to @post, notice: 'Post was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /posts/1
  def update
    if @post.update(post_params)
      redirect_to @post, notice: 'Post was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /posts/1
  def destroy
    @post.destroy
    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      account = current_account || current_oauth_account
      @post = account.posts.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def post_params
      params.require(:post).permit(:title, :body)
    end

    def require_read_access
      return require_authentication unless request.authorization && request.authorization.start_with?("Bearer")

      rodauth.require_oauth_authorization("posts.read")
    end
end
