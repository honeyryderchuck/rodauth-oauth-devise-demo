class RodauthApp < Rodauth::Rails::App
  PRIV_KEY = OpenSSL::PKey::RSA.new(File.read(Rails.root.join("tmp", "rsaprivkey.pem")))
  PUB_KEY = OpenSSL::PKey::RSA.new(File.read(Rails.root.join("tmp", "rsapubkey.pem")))

  configure do
    # List of authentication features that are loaded.
    enable :oidc, :oidc_dynamic_client_registration, :oauth_application_management

    # See the Rodauth documentation for the list of available config options:
    # http://rodauth.jeremyevans.net/documentation.html

    # ==> General
    # The secret key used for hashing public-facing tokens for various features.
    # Defaults to Rails `secret_key_base`, but you can use your own secret key.
    # hmac_secret "48bcab34b0043580a20354365a8f0ea1cabc54d0f3cadf4bd75b56be0a25a6fc7e7eac72fcaab720a47db8a4895af849e01ecf2a29eb24597c9ae8728a570f2c"

    # Specify the controller used for view rendering and CSRF verification.
    rails_controller { RodauthController }

    accounts_table :users

    require_login_redirect { "/users/sign_in" }

    # list of OIDC and OAuth scopes you handle
    oauth_application_scopes %w[openid email profile posts.read]

    oauth_account_ds { |id| User.where(account_id_column => id) }
    oauth_application_ds { |id| OAuthApplication.where(oauth_applications_id_column => id) }

    # by default you're only allowed to use https redirect URIs. But we're developing,
    # so it's fine.
    if Rails.env.development?
      oauth_valid_uri_schemes %w[http https]
    end

    oauth_jwt_keys("RS256" => PRIV_KEY)
    oauth_jwt_public_keys("RS256" => PUB_KEY)

    # this callback is executed when gathering OIDC claims to build the
    # ID token with.
    # You should return the values for each of these claims.
    #
    # This callback is called in a loop for all available claims, so make sure
    # you memoize access to to the database models to avoid the same query
    # multiple times.
    get_oidc_param do |account, param|
      @user ||= User.find_by(id: account[:id])
      case param
      when :email
        @user.email
      when :email_verified
        true
      when :name
        @user.name
      end
    end

    before_register do
      secret_code = request.env["HTTP_AUTHORIZATION"]
      authorization_required unless secret_code = "12345678"
      @oauth_application_params[:account_id] = _account_from_login("foo@bar.com")[:id]
    end

    # Store account status in a text column.
    # account_status_column :status
    # account_unverified_status_value "unverified"
    # account_open_status_value "verified"
    # account_closed_status_value "closed"

    # Store password hash in a column instead of a separate table.
    # account_password_hash_column :password_digest

    # Set password when creating account instead of when verifying.
    # verify_account_set_password? false

    # Redirect back to originally requested location after authentication.
    # login_return_to_requested_location? true
    # two_factor_auth_return_to_requested_location? true # if using MFA

    # Autologin the user after they have reset their password.
    # reset_password_autologin? true

    # Delete the account record when the user has closed their account.
    # delete_account_on_close? true

    # Redirect to the app from login and registration pages if already logged in.
    # already_logged_in { redirect login_redirect }

    # ==> Flash
    # Match flash keys with ones already used in the Rails app.
    # flash_notice_key :success # default is :notice
    # flash_error_key :error # default is :alert

    # Override default flash messages.
    # create_account_notice_flash "Your account has been created. Please verify your account by visiting the confirmation link sent to your email address."
    # require_login_error_flash "Login is required for accessing this page"
    # login_notice_flash nil

    # ==> Validation
    # Override default validation error messages.
    # no_matching_login_message "user with this email address doesn't exist"
    # already_an_account_with_this_login_message "user with this email address already exists"
    # password_too_short_message { "needs to have at least #{password_minimum_length} characters" }
    # login_does_not_meet_requirements_message { "invalid email#{", #{login_requirement_message}" if login_requirement_message}" }

    # Change minimum number of password characters required when creating an account.
    # password_minimum_length 8


    # Or only remember users that have ticked a "Remember Me" checkbox on login.
    # after_login { remember_login if param_or_nil("remember") }

    # ==> Hooks
    # Validate custom fields in the create account form.
    # before_create_account do
    #   throw_error_status(422, "name", "must be present") if param("name").empty?
    # end

    # Perform additional actions after the account is created.
    # after_create_account do
    #   Profile.create!(account_id: account_id, name: param("name"))
    # end

    # Do additional cleanup after the account is closed.
    # after_close_account do
    #   Profile.find_by!(account_id: account_id).destroy
    # end

    # ==> Deadlines
    # Change default deadlines for some actions.
    # verify_account_grace_period 3.days
    # reset_password_deadline_interval Hash[hours: 6]
    # verify_login_change_deadline_interval Hash[days: 2]
    # remember_deadline_interval Hash[days: 30]

    logged_in? { rails_controller_instance.user_signed_in? }
    session_value { rails_controller_instance.session.fetch("warden.user.user.key", []).dig(0, 0) || super() }
    get_oidc_account_last_login_at { |user_id| User.find(user_id).last_sign_in_at }
  end

  # ==> Secondary configurations
  # configure(:admin) do
  #   # ... enable features ...
  #   prefix "/admin"
  #   session_key_prefix "admin_"
  #   # remember_cookie_key "_admin_remember" # if using remember feature
  #
  #   # search views in `app/views/admin/rodauth` directory
  #   rails_controller { Admin::RodauthController }
  # end


  route do |r|
    r.rodauth # route rodauth requests
    rodauth.load_oauth_application_management_routes
    rodauth.load_openid_configuration_route
    rodauth.load_webfinger_route

    # ==> Authenticating Requests
    # Call `rodauth.require_authentication` for requests that you want to
    # require authentication for. Some examples:
    #
    # next if r.path.start_with?("/docs") # skip authentication for documentation pages
    # next if session[:admin] # skip authentication for admins
    #
    # # authenticate /dashboard/* and /account/* requests
    # if r.path.start_with?("/dashboard") || r.path.start_with?("/account")
    #   rodauth.require_authentication
    # end

    # ==> Secondary configurations
    # r.on "admin" do
    #   r.rodauth(:admin)
    #
    #   unless rodauth(:admin).logged_in?
    #     rodauth(:admin).require_http_basic_auth
    #   end
    #
    #   break # allow the Rails app to handle other "/admin/*" requests
    # end
  end
end
